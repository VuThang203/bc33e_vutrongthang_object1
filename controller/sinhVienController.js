function layThongTinTuForm() {
  console.log("layThongTinTuForm");
  var maSV = document.getElementById("txtMaSV").value.trim();
  var tenSV = document.getElementById("txtTenSV").value.trim();
  var email = document.getElementById("txtEmail").value.trim();
  var matKhau = document.getElementById("txtPass").value.trim();
  var diemToan = document.getElementById("txtDiemToan").value;
  var diemLy = document.getElementById("txtDiemLy").value;
  var diemHoa = document.getElementById("txtDiemHoa").value;

  var sv = new SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa);
  // console.log("sv : ", sv.tinhDTB());
  return sv;
}

function renderDanhSachSinhVien(list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var content = `<tr>
      <td>${item.ma}</td>
      <td>${item.ten}</td>
      <td>${item.email}</td>
      <td>${item.tinhDTB()}</td>
      <td>
      <button onclick = "xoaSV('${
        item.ma
      }')" class="btn btn-danger">Xoá</button>
      <button onclick = "suaSV('${
        item.ma
      }')" class="btn btn-warning">Sửa</button>
      </td>
      </tr>`;
    contentHTML += content;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
  // for (var index = 0; index < arr.length; index++) {
  //   var sv = arr[index];
  //   if (sv.ma == id) {
  //     return index;
  //   }
  // }
  // return -1;

  // let index = arr.findIndex(function (sv) {
  //   return sv.ma == id;
  // });
  // return index;

  return arr.findIndex(function (sv) {
    return sv.ma == id;
  });
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

var dog1 = {
  name: "miu",
  age: 3,
};
var dog2 = {
  dogname: "cat",
  age: 4,
};

function Dog(ten, tuoi) {
  this.name = ten;
  this.age = tuoi;
}
var dog3 = new Dog("den", 5);
dog3.name;
dog3.age;
var arrDog = [dog1, dog2];
console.log("dog3: ", dog3);
