var validator = {
  kiemTraRong: function (valueInput, idError, messenger) {
    if (valueInput.trim() == "") {
      document.getElementById(idError).innerText = messenger;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraMaSV: function (maSV, dssv) {
    var index = dssv.findIndex((sv) => {
      return sv.ma == maSV;
    });
    console.log("index: ", index);
    if (index !== -1) {
      document.getElementById("spanMaSV").innerText = "Mã sinh viên đã tồn tại";
      return false;
    } else {
      document.getElementById("spanMaSV").innerText = "";
      return true;
    }
  },
  kiemTraDoDai: function (valueInput, idError, min, max) {
    var inputLength = valueInput.length;
    if (inputLength > max || inputLength < min) {
      document.getElementById(
        idError
      ).innerText = `Độ dài phải từ ${min} - ${max} kí tự`;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraChuoiSo: function () {
    var regex = new Regex(/^[0-9]+$/);
    if (regex.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Trường này chỉ được nhập số";

      return false;
    }
  },
};
