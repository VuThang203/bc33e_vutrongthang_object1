var dssv = [];
const DSSV = "DSSV";
// Lấy dssv từ localstorage dưới dạng json
let dssvLocalStorage = localStorage.getItem(DSSV);
console.log("dssvLocalStorage: ", JSON.parse(dssvLocalStorage));
// convert json thành array và gán lại cho dssv
if (JSON.parse(dssvLocalStorage)) {
  var data = JSON.parse(dssvLocalStorage);
  console.log("before: ", data);

  for (var index = 0; index < data.length; index++) {
    var current = data[index];
    var sv = new SinhVien(
      current.ma,
      current.ten,
      current.email,
      current.matKhau,
      current.diemToan,
      current.diemLy,
      current.diemHoa
    );
    dssv.push(sv);
  }
  renderDanhSachSinhVien(dssv);
  console.log("after: ", dssv);
}

function saveLocalStorage() {
  // convert array thanh json
  var dssvJson = JSON.stringify(dssv);
  console.log("dssvJson: ", dssvJson);
  // Lưu xuống local storage
  localStorage.setItem(DSSV, dssvJson);
}

function themSV() {
  console.log("yes");
  var newSV = layThongTinTuForm();

  //validate mã sv

  var isValid =
    validator.kiemTraRong(newSV.ma, "spanMaSV", "Bất ngờ chưa!") &&
    validator.kiemTraMaSV(newSV.ma, dssv) &&
    validator.kiemTraDoDai(newSV.ma, "spanMaSV", 6, 8) &&
    validator.kiemTraChuoiSo(newSV.ma, "spanMaSV");

  // validate tên sv
  isValid =
    isValid & validator.kiemTraRong(newSV.ten, "spanTenSV", "Bất ngờ chưa!");
  // validator.kiemTraRong(newSV.ten, "spanTenSV", "Bất ngờ chưa!") &
  // validator.kiemTraRong(newSV.email, "spanEmailSV", "Bất ngờ chưa!") &
  // validator.kiemTraRong(newSV.matKhau, "spanMatKhau", "Bất ngờ chưa!") &
  // validator.kiemTraRong(newSV.diemToan, "spanToan", "Bất ngờ chưa!") &
  // validator.kiemTraRong(newSV.diemLy, "spanLy", "Bất ngờ chưa!") &
  // validator.kiemTraRong(newSV.diemHoa, "spanHoa", "Bất ngờ chưa!") &

  if (isValid == false) {
    return;
  }
  // newSV.ma = shortid.generate();
  dssv.push(newSV);
  console.log("dssv: ", dssv);

  //   Xuất danh sách:
  renderDanhSachSinhVien(dssv);
}
function xoaSV(id) {
  // console.log("id: ", id);
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index !== -1) {
    // console.log("bf: ", dssv.length);
    dssv.splice(index, 1);
    // console.log("at: ", dssv.length);
    saveLocalStorage();
    renderDanhSachSinhVien(dssv);
  }
}

function suaSV(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index !== -1) {
    var sv = dssv[index];
    saveLocalStorage();
    showThongTinLenForm(sv);
  }
}

function capNhatSV() {
  var svEdited = layThongTinTuForm();
  console.log("svEdited: ", svEdited);

  let index = timKiemViTri(svEdited.ma, dssv);

  if (index !== -1) {
    dssv[index] = svEdited;
    saveLocalStorage();
    renderDanhSachSinhVien(dssv);
  }
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
